package com.reddit_clone.mini_project_001.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentsClass {

    private int cmt_id;
    private String cmttitle;
    private int like_count;
    private int post_id;
    private Date auto_date;
    private Date update_date;


}
