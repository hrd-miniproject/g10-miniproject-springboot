package com.reddit_clone.mini_project_001.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ImageModel {
    private MultipartFile file;
    private String imgurl;
    private  int id;
}
