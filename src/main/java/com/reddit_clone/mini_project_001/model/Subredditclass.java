package com.reddit_clone.mini_project_001.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Subredditclass {
    private  int id ;
    private  String title ;
    private Date auto_date;
    private  Date update_date;
}
