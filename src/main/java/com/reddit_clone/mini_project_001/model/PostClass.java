package com.reddit_clone.mini_project_001.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class PostClass {


    private MultipartFile file;
    private int post_id;
    private String post_title;
    private int like_count ;
    private  String url;
    private  String description;
    private  int subred_id;
    private  Date auto_date;
    private  Date update_date;
    private List<CommentsClass> cmt;
    private Subredditclass subred;

}
