package com.reddit_clone.mini_project_001.controller;


import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.PostClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import com.reddit_clone.mini_project_001.service.PostService;
import com.reddit_clone.mini_project_001.service.SubredditService;
import com.reddit_clone.mini_project_001.service.FileStorageService;
import com.reddit_clone.mini_project_001.service.serviceimp.CommentsServiceImp;
import com.reddit_clone.mini_project_001.service.serviceimp.PostServiceImp;
import org.apache.tomcat.util.net.jsse.JSSEUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Controller
public class SubredditController {

    @Autowired
    SubredditService subredditService;

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    PostServiceImp postService;

    @Autowired
    CommentsServiceImp commentsServiceImp;



    @GetMapping("/")
    public String index(Model model){
        List<Subredditclass> subredditclassList = subredditService.getSubredditData();
        model.addAttribute("subreddit",subredditclassList);
        model.addAttribute("postclassobj",new PostClass());
        model.addAttribute("postclass",postService.getPostData());

        return "index";
    }

    @GetMapping("/createPost")
    public String createPost(Model model) {
        model.addAttribute("insertpost",new PostClass());
        model.addAttribute("subredditobj",new Subredditclass());
        model.addAttribute("Subredditdata",subredditService.getSubredditData());
        return "CreatePost";
    }

    @GetMapping("/readPost/{post_id}")
    public String readPost(Model model,@PathVariable int post_id ) {
        PostClass postClass = postService.findPostById(post_id);
        List<CommentsClass> commentsClassList = commentsServiceImp.getCommentData() ;
        model.addAttribute("postClass",postClass);
        model.addAttribute("loopcmt",commentsServiceImp.getCommentData());
        model.addAttribute("comment",new CommentsClass());
        return "ReadPost";
    }

    @GetMapping("/updatePost/{post_id}")
    public String updatePost(Model model,@PathVariable int post_id ){
        PostClass postClass = postService.findPostById(post_id);
        model.addAttribute("postClass",postClass);
        model.addAttribute( "subredditobj ",new Subredditclass());
        model.addAttribute("subreddit",postClass.getSubred().getTitle());

        return "UpdatePost";
    }

    @GetMapping("/editSubreddit/{id}")
    public String subEdit(@PathVariable("id") int id,Model model){
        model.addAttribute("subreddit",subredditService.findSubRedditByid(id));
        return "UpdateSubreddit";
    }

    @PostMapping("/updateSubreddit/{id}")
    public String updateSub(@ModelAttribute Subredditclass  subreddit,  @PathVariable int id)
    {
        LocalDate date=LocalDate.now();
        subreddit.setId(id);
        subreddit.setUpdate_date(new Date(System.currentTimeMillis()));
        System.out.println("here is new subreddit"+subreddit);
        subredditService.updateSubById(subreddit.getId(), subreddit.getTitle(), date);
        return "redirect:/";

    }

    @PostMapping("/handle-comment/{post_id}")
    public String handleComment(@ModelAttribute CommentsClass commentsClass,@PathVariable int post_id){
        commentsServiceImp.insertComment(commentsClass);

        return "redirect:/readPost/"+post_id;
    }

    @PostMapping("/handle-update/{post_id}")
    public String handleUpdate(@ModelAttribute PostClass postClass,@PathVariable int post_id){

        try{
            if(postClass.getFile().isEmpty()){

            }else {
                try {
                    String filename = "http://localhost:8081/images/"+ fileStorageService.saveFile(postClass.getFile());
                    postClass.setUrl(filename);

                    System.out.println("New Profile : "+postClass.getUrl());
                }catch (Exception e){
                    System.out.println("Error with the images upload : " + e.getMessage());
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        postService.updatePost(post_id,postClass);

     return "redirect:/";

    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid PostClass postClass, BindingResult bindingResult, Model model){
        try{
            if(postClass.getFile().isEmpty()){
            }else {
                try {
                    String filename = "http://localhost:8081/images/"+ fileStorageService.saveFile(postClass.getFile());
                    postClass.setUrl(filename);
                }catch (Exception e){
                    System.out.println("Error with the images upload : " + e.getMessage());
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        if(bindingResult.hasErrors()){
            System.out.println("BindingResult error");
            model.addAttribute("allSubreddit", subredditService.getSubredditData());
            return "CreatePost";
        }
        postService.insertToPost(postClass);
        return "redirect:/";

    }


    @GetMapping("/deletePost{post_id}")
    public String deletePost(Model model ,@PathVariable("post_id") int id ){

        model.addAttribute("delete",postService.deletePost(id));
        return "redirect:/";
    }

    @GetMapping("/deleteSubReddit{id}")
    public String deleteSubReddit(Model model, @PathVariable("id") int id ){
        model.addAttribute("deleteSubReddit",subredditService.deleteSubreddit(id));
        return "redirect:/viewAllSubReddit";
    }

    //////////////////////////////////////////////////////////////////////

    @GetMapping("/createSubreddit")
    public String createSubReddit(Model model) {
        model.addAttribute("subreddit",new Subredditclass());
        return "CreateSubReddit";
    }

    @PostMapping("/addSubreddits")
    public String addSubReddit(@ModelAttribute Subredditclass subredditclass){
        subredditService.insertSubReddit( subredditclass);
        return "redirect:/";
    }

    @GetMapping("/viewSubReddit/{post_id}")
    public String viewSubReddit(Model model,@PathVariable("post_id") int post_id){
        subredditService.findSubRedditByid(post_id);
        List<Subredditclass> subredditclassList = subredditService.getSubredditData();
        model.addAttribute("subreddit",subredditclassList);
        model.addAttribute("postclassobj",new PostClass());
        model.addAttribute("postclass",postService.getPostData());


        return "viewSubReddit";
    }

    @GetMapping("/viewAllSubReddit")
    public String viewAllSubReddit(Model model){
        model.addAttribute("allSubReddit",subredditService.getSubredditData());

        return "/viewAllSubReddit";
    }

    //config like and dislike
    @GetMapping("/like_Count/{id}")
    public String likepost(@PathVariable("id") int id){
        int like =postService.getPostLikeByID(id);
        if(like<1){
            like=1;
        }else
        {
            like=like-1;
        }

        postService.IncreaseLikePost(id,(like));
        return "redirect:/";
    }

    @GetMapping("/unlike_count_In_Comment/{id}")
    public String dislikepostInComment(@PathVariable("id") int id) {
        int like = postService.getPostLikeByID(id);
        if (like >= 0) {
            like = -1;
        } else if (like < 0) {
            like = 0;
        }
        postService.IncreaseLikePost(id, (like));
        return "redirect:/readPost/" + id;
    }

    @GetMapping("/like_Count_In_Comment/{id}")
    public String likepostInComment(@PathVariable("id") int id){
        int like =postService.getPostLikeByID(id);
        if(like<1){
            like=1;
        }else
        {
            like=like-1;
        }

        postService.IncreaseLikePost(id,(like));
        return "redirect:/readPost/" + id;

    }

    @GetMapping("/unlike_count/{id}")
    public String dislikepost(@PathVariable("id") int id){
        int like=postService.getPostLikeByID(id);
        if(like>=0){
            like=-1;
        }else if(like<0)
        {
            like=0;
        }
        postService.IncreaseLikePost(id,(like));
        return "redirect:/";
    }




}
