package com.reddit_clone.mini_project_001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class MiniProject001Application {

    public static void main(String[] args) {
        SpringApplication.run(MiniProject001Application.class, args);
    }

}
