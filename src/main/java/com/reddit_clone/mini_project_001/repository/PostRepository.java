package com.reddit_clone.mini_project_001.repository;

import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.PostClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface PostRepository {

        //get post information  by post id //
        @Select("select * from postingtb order by post_id")
        @Results(
                {
                        @Result(property = "post_id", column = "post_id"),
                        @Result(property = "post_title", column = "post_title"),
                        @Result(property = "like_count", column = "like_count"),
                        @Result(property = "url", column = "url"),
                        @Result(property = "description", column = "description"),
                        @Result(property = "subred", column = "subred_id", many = @Many(select = "findSubredditById")),
                        @Result(property = "cmt", column = "post_id", many = @Many(select = "findCommentById")),
                        @Result(property = "auto_date", column = "auto_date"),
                        @Result(property = "update_date", column = "update_date")
                }
        )
        public List<PostClass> getPostData();


        ////Find subbyid
        @Select("select title from subreddittb where id =#{id}")
        public Subredditclass findTitleSubredditById(int id);


        ///Find Comment By id //


    @Select("select * from postingtb where post_id = #{post_id}")
    @Results(
            {
                    @Result(property = "post_id", column = "post_id"),
                    @Result(property = "post_title", column = "post_title"),
                    @Result(property = "like_count", column = "like_count"),
                    @Result(property = "url", column = "url"),
                    @Result(property = "description", column = "description"),
                    @Result(property = "subred_id",column = "subred_id"),
                    @Result(property = "subred", column = "subred_id", many = @Many(select = "findSubredditById")),
                    @Result(property = "cmt", column = "post_id", many = @Many(select = "findCommentById")),
                    @Result(property = "create_date", column = "create_date"),
                    @Result(property = "update_date", column = "update_date")
            }
    )
    public PostClass findPostById(int post_id);

    ////Find subbyid
    @Select("select * from subreddittb where id =#{subred_id}")
    public Subredditclass findSubredditById(int subred_id);

    @Select("select * from commenttb where post_id=#{post_id}")
    public List<CommentsClass> findCommentById(int post_id);


        ////////////insert post information /////////////////


    @Select("insert into postingtb(post_title, like_count  , url, description, subred_id) " +
            "values (#{postClass.post_title},#{postClass.like_count},#{postClass.url},#{postClass.description}, #{postClass.subred_id}) returning post_id")
    int insertToPost(@Param("postClass") PostClass postClass);



    //update post//
    @Update("update postingtb set post_title=#{postClass.post_title}, like_count=#{postClass.like_count}, url=#{postClass.url}, description=#{postClass.description}, subred_id=#{postClass.subred_id} where post_id=#{id}")
    public Boolean updatePost(int id, @Param("postClass") PostClass postClass);



    //delete post//
    @Delete("delete from postingtb where post_id=#{id}")
    public Boolean deletePost(int id);

    //////// like ///////////////

    @Select("Select like_count from postingtb where post_id=#{id}")
    public int getPostLikeByID(int id);

    @Update("UPDATE postingtb set like_count =#{newLike} where post_id=#{id}")
    public int IncreaseLikePost(int id,int newLike);

    @Select("Select like_count from postingtb where post_id=#{id}")
    public int getPostLikeByIDInComment(int id);

    @Update("UPDATE postingtb set like_count =#{newLike} where post_id=#{id}")
    public int IncreaseLikePostInComment(int id,int newLike);


}


