package com.reddit_clone.mini_project_001.repository;

import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.PostClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface SubredditRepository {
    // For Get  Subreddit Data//
    @Select("select * from  subreddittb")
    public List<Subredditclass> getSubredditData();

    // For Insert Into Subreddit Table//
    @Insert("insert into subreddittb (title) values (#{subredditclass.title})")
        public  int insertSubreddit(@Param("subredditclass") Subredditclass subredditclass);

    ///////////////////////////////// find the Comment by id /////
    @Select("select * from CommentsClass where post_id=#{id_post}")
    public List<CommentsClass> findCmtByid(int id_post);


    /////////Find Data of Renddit by id ///////////////
    @Select("select * from subreddittb where id =#{id}")
    public Subredditclass findSubRedditByid(int id);


            @Update("update subreddittb set title=#{subredditclass.title} where id=#{subredditclass.id}")
    public int updateSubreddit(@Param("subredditclass")Subredditclass subredditclass);

    @Update("update subreddittb set title=#{title},update_date=#{update_date} where id=#{id}")
    public int updateSubById(int id, String title, LocalDate update_date);

    @Delete("delete from subreddittb where id=#{id}")
    public int deleteSubreddit(int id );



}
