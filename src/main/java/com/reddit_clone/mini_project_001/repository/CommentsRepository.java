package com.reddit_clone.mini_project_001.repository;

import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface CommentsRepository {

// for get Comment Data //
    @Select("select * from commenttb")
    @Results(
            {
                    @Result(property = "cmt_id" , column = "id"),
                    @Result(property = "cmttitle" , column = "title")
            }
    )
    public List<CommentsClass> getCommentData();

    // for insert comment data //
    @Insert("insert into  commenttb (title,like_count,post_id) values (#{commentsClass.cmttitle},#{commentsClass.like_count},#{commentsClass.post_id})")
    public int insertComment(@Param("commentsClass") CommentsClass commentsClass);



    // update comment data
     @Update("update commenttb set title=#{commentsClass.cmttitle} where id=#{commentsClass.id}")
        public  int updateComment(@Param("commentsClass") CommentsClass commentsClass);


     // delete Comment //
    @Delete(" delete from commenttb where id=#{id}")
    public int deleteComment(int id );

}
