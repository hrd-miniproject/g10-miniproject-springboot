package com.reddit_clone.mini_project_001.service;
import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.PostClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public interface SubredditService {


                // get title from subreddit by auto date and update date auto
    public List<Subredditclass> getSubredditData();

                // input data to subreddit only name by auto id auto date and auto update date  //
    public int insertSubReddit(Subredditclass subredditclass);

                /// search subreddit by id /////
    public Subredditclass findSubRedditByid(int id);
                ///////Find Comment By Id //////////////////////////
    public List<CommentsClass> findCmtByid(int id_post);
                ///// Update Subreddit Data ////
    public  int updateSubreddit(Subredditclass subredditclass);

    ///// Delete Subreddit Data ////
    public int deleteSubreddit(int id );

    public int updateSubById(int id, String title, LocalDate update_date);







}
