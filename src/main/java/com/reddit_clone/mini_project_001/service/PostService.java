package com.reddit_clone.mini_project_001.service;

import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.PostClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {

    public List<PostClass> getPostData();
    public PostClass findPostById(int post_id);

    public  int insertToPost( PostClass postClass);

    public Subredditclass findSubredditById(int id);
    public List<CommentsClass> findCommentById(int post_id);
    public Boolean deletePost(int id);
    public Boolean updatePost(int id, @Param("postClass") PostClass postClass);

    public int getPostLikeByID(int id);

    public int IncreaseLikePost(int id, int newLike);

    public int getPostLikeByIDInComment(int id);
    public int IncreaseLikePostInComment(int id, int newLike);

}
