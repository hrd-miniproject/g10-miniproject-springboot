package com.reddit_clone.mini_project_001.service;

import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import com.reddit_clone.mini_project_001.repository.CommentsRepository;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentsService {


    public List<CommentsClass> getCommentData();

    public int insertComment(CommentsClass commentsClass);

    public int deleteComment(int id );

    public  int updateComment( CommentsClass commentsClass);


}
