package com.reddit_clone.mini_project_001.service.serviceimp;


import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.repository.CommentsRepository;
import com.reddit_clone.mini_project_001.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentsServiceImp  implements CommentsService {

    @Autowired
    CommentsRepository commentsRepository;

    @Override
    public List<CommentsClass> getCommentData() {
        return  commentsRepository.getCommentData();
    }

    @Override
    public int insertComment(CommentsClass commentsClass) {
        return commentsRepository.insertComment(commentsClass);
    }

    @Override
    public int deleteComment(int id) {
        return commentsRepository.deleteComment(id);
    }

    @Override
    public int updateComment(CommentsClass commentsClass) {
        return commentsRepository.updateComment(commentsClass);
    }

}
