package com.reddit_clone.mini_project_001.service.serviceimp;

import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import com.reddit_clone.mini_project_001.repository.SubredditRepository;
import com.reddit_clone.mini_project_001.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class SubServiceImp implements SubredditService {

    // call to repo: inject
    @Autowired
    SubredditRepository subredditRepository;


    @Override
    public List<Subredditclass> getSubredditData() {
        return subredditRepository.getSubredditData();
    }

    @Override
    public int updateSubById(int id, String title, LocalDate update_date) {
        return subredditRepository.updateSubById(id,title,update_date);
    }

    @Override
    public int insertSubReddit(Subredditclass subredditClass) {
        return subredditRepository.insertSubreddit(subredditClass);
    }

    @Override
    public Subredditclass findSubRedditByid(int id) {
        return subredditRepository.findSubRedditByid(id);
    }

    @Override
    public List<CommentsClass> findCmtByid(int id_post) {
        return findCmtByid(id_post);
    }

    @Override
    public int updateSubreddit(Subredditclass subredditclass) {
        return subredditRepository.updateSubreddit(subredditclass);
    }

    @Override
    public int deleteSubreddit(int id) {
        return subredditRepository.deleteSubreddit(id);
    }

}
