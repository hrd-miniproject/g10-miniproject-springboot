package com.reddit_clone.mini_project_001.service.serviceimp;


import com.reddit_clone.mini_project_001.model.CommentsClass;
import com.reddit_clone.mini_project_001.model.PostClass;
import com.reddit_clone.mini_project_001.model.Subredditclass;
import com.reddit_clone.mini_project_001.repository.PostRepository;
import com.reddit_clone.mini_project_001.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {


    @Autowired
    PostRepository postRepository;

    @Override
    public List<PostClass> getPostData() {
        return postRepository.getPostData();
    }

    @Override
    public PostClass findPostById(int post_id) {
        return postRepository.findPostById(post_id);
    }

    @Override
    public int insertToPost(PostClass postClass) {
        return postRepository.insertToPost(postClass );
    }

    @Override
    public Subredditclass findSubredditById(int id) {
        return postRepository.findSubredditById(id);
    }

    @Override
    public List<CommentsClass> findCommentById(int post_id) {
        return postRepository.findCommentById(post_id);
    }

    @Override
    public Boolean deletePost(int id) {
        return postRepository.deletePost(id);
    }

    @Override
    public Boolean updatePost(int id, PostClass postClass) {
        return postRepository.updatePost(id,postClass);
    }

    @Override
    public int getPostLikeByID(int id) {
        return postRepository.getPostLikeByID(id);
    }
    @Override
    public int IncreaseLikePost(int id, int newLike) {
        return postRepository.IncreaseLikePost(id, newLike);
    }

    @Override
    public int getPostLikeByIDInComment(int id) {
        return postRepository.getPostLikeByIDInComment(id);
    }

    @Override
    public int IncreaseLikePostInComment(int id, int newLike) {
        return postRepository.IncreaseLikePostInComment(id,newLike);
    }
}
