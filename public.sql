/*
 Navicat Premium Data Transfer

 Source Server         : G10_Mini_project_Spring_001
 Source Server Type    : PostgreSQL
 Source Server Version : 140002
 Source Host           : localhost:5432
 Source Catalog        : Reddit_Clone
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140002
 File Encoding         : 65001

 Date: 15/05/2022 15:02:18
*/


-- ----------------------------
-- Sequence structure for comments_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."comments_id_seq";
CREATE SEQUENCE "public"."comments_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_post_id_seq";
CREATE SEQUENCE "public"."post_post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sub_reddit_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sub_reddit_id_seq";
CREATE SEQUENCE "public"."sub_reddit_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for commenttb
-- ----------------------------
DROP TABLE IF EXISTS "public"."commenttb";
CREATE TABLE "public"."commenttb" (
  "id" int4 NOT NULL DEFAULT nextval('comments_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "auto_date" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "like_count" int4,
  "post_id" int4,
  "update_date" date
)
;

-- ----------------------------
-- Records of commenttb
-- ----------------------------
INSERT INTO "public"."commenttb" VALUES (3, 'that is awesome ', '2022-05-14', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for postingtb
-- ----------------------------
DROP TABLE IF EXISTS "public"."postingtb";
CREATE TABLE "public"."postingtb" (
  "post_id" int4 NOT NULL DEFAULT nextval('post_post_id_seq'::regclass),
  "post_title" varchar(255) COLLATE "pg_catalog"."default",
  "like_count" int4,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "subred_id" int4,
  "auto_date" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_date" date
)
;

-- ----------------------------
-- Records of postingtb
-- ----------------------------
INSERT INTO "public"."postingtb" VALUES (2, 'testing ', NULL, NULL, 'it''s fun ', NULL, '2022-05-15', NULL);
INSERT INTO "public"."postingtb" VALUES (10, 'hello testing 1 ', 0, NULL, 'this is the first testing program ', NULL, '2022-05-15', NULL);

-- ----------------------------
-- Table structure for subreddittb
-- ----------------------------
DROP TABLE IF EXISTS "public"."subreddittb";
CREATE TABLE "public"."subreddittb" (
  "id" int4 NOT NULL DEFAULT nextval('sub_reddit_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "auto_date" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_date" date
)
;

-- ----------------------------
-- Records of subreddittb
-- ----------------------------
INSERT INTO "public"."subreddittb" VALUES (27, 'dsfdfd', '2022-05-14', '2022-05-14');
INSERT INTO "public"."subreddittb" VALUES (28, 'dsfdfd', '2022-05-14', '2022-05-14');
INSERT INTO "public"."subreddittb" VALUES (29, 'dsfdfd', '2022-05-14', '2022-05-14');

-- ----------------------------
-- Function structure for trigger_set_timestamp
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."trigger_set_timestamp"();
CREATE OR REPLACE FUNCTION "public"."trigger_set_timestamp"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
  NEW.update_date = NOW();
  RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."comments_id_seq"
OWNED BY "public"."commenttb"."id";
SELECT setval('"public"."comments_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."post_post_id_seq"
OWNED BY "public"."postingtb"."post_id";
SELECT setval('"public"."post_post_id_seq"', 10, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."sub_reddit_id_seq"
OWNED BY "public"."subreddittb"."id";
SELECT setval('"public"."sub_reddit_id_seq"', 29, true);

-- ----------------------------
-- Triggers structure for table commenttb
-- ----------------------------
CREATE TRIGGER "set_timestamp" BEFORE UPDATE ON "public"."commenttb"
FOR EACH ROW
EXECUTE PROCEDURE "public"."trigger_set_timestamp"();

-- ----------------------------
-- Primary Key structure for table commenttb
-- ----------------------------
ALTER TABLE "public"."commenttb" ADD CONSTRAINT "comments_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table postingtb
-- ----------------------------
CREATE TRIGGER "set_timestamp" BEFORE UPDATE ON "public"."postingtb"
FOR EACH ROW
EXECUTE PROCEDURE "public"."trigger_set_timestamp"();

-- ----------------------------
-- Primary Key structure for table postingtb
-- ----------------------------
ALTER TABLE "public"."postingtb" ADD CONSTRAINT "post_pkey" PRIMARY KEY ("post_id");

-- ----------------------------
-- Triggers structure for table subreddittb
-- ----------------------------
CREATE TRIGGER "set_timestamp" BEFORE UPDATE ON "public"."subreddittb"
FOR EACH ROW
EXECUTE PROCEDURE "public"."trigger_set_timestamp"();

-- ----------------------------
-- Primary Key structure for table subreddittb
-- ----------------------------
ALTER TABLE "public"."subreddittb" ADD CONSTRAINT "sub_reddit_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table commenttb
-- ----------------------------
ALTER TABLE "public"."commenttb" ADD CONSTRAINT "post_id" FOREIGN KEY ("post_id") REFERENCES "public"."postingtb" ("post_id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table postingtb
-- ----------------------------
ALTER TABLE "public"."postingtb" ADD CONSTRAINT "subreddit_id" FOREIGN KEY ("subred_id") REFERENCES "public"."subreddittb" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
